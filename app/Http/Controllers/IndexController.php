<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\GetShockProducts;
use App\FilterByDomain;
use DB;
use App\Http\Resources\Set as SetResource;
use App\User;


class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index() {
        $user = Auth::user();

        $select = 'SELECT sales_code
        ,sales_netsales
        ,sales_product_name
        ,sales_reactive
        ,customer_counter
        ,customer_fname
        ,customer_lname
        ,sales_date
        ,sales_date_control
        ,sales_control
        ,sales_ordertype
        ,sales_product
        ,customer_email 
        FROM customers
        ,sales 
        WHERE customer_code=sales_customers 
        AND sales_customers='.$user->customer_code;
        
        $products = DB::select($select,[1]);
        
        define ( 'DATE_LIMIT', '15-05-2015' ); //24 hour from 14 may 2015
        //This constant is defined because is the new range date, this apply for new  dat structure subscription
        define ( 'DATE_UNIX', 1431648000 ); //Date unix from the old customers sales
        
        
        foreach ($products as $product)
        {
            $sales_date = 0;
            $sales_date = strtotime ( $product->sales_date ); //sales_data in time stamp
            $ct_email = $product->customer_email;
            if ( DATE_UNIX > $sales_date) 
                $date_query =  DATE_UNIX;
            else
                $date_query = $sales_date;
    
            $sales_netsales = intval( $product->sales_netsales );
    
            
            if ( $sales_netsales >= 99)
            {
                $date_query = time();
                $isfinder = true;
            }
    
            $date_format = date( "Y-m-d H:i:s", $date_query ); /// like atom style
    
            
            switch ($product->sales_ordertype) {
                case 'sh':
                    $date_format = date( "Y-m-d H:i:s" );
                    $cgroos = intval($product->sales_netsales);
                    $shk = '';
                    $sales_date = strtotime($product->sales_date);
    
                    switch( $cgroos ){
                        case 14: // shk subscription monthly
                            $date_expired = strtotime('+1 month', $sales_date );
                        break;
                        case 129: // shk subscription yearly
                            $date_expired = strtotime('+1 year', $sales_date );
                        break;
                    }
    
                    $purchase_history[] = array(
                                                'product_name' => $product->sales_product_name,
                                                'price' => $product->sales_netsales,
                                                'sales_date' => $product->sales_date,
                                                'expired' => $date_expired
                                               );
    
                    if ( $date_expired < $_SERVER['REQUEST_TIME'] ) {///verified time expired
                        $msgexp = <<<msgexp
        <p class="notify">Your ShockFamily subscription has expired. In order to continue, downloading current and new products, please renew your account here:</p>
        <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick-subscriptions&business=fabbianni%40gmail.com&item_name=ShockFamily+Monthly+Subscription&currency_code=USD&notify_url=http%3A%2F%2Fwww.themeshock.com%2Fst_subscription_payment.php&t3=M&src=1&sra=1&modify=0&a1=14&p1=1&t1=M&a3=14&no_note=1&p3=1&no_shipping=1&return=http%3A%2F%2Fwww.themeshock.com%2Fcustomers%2Fcustomers.php&custom=aff_%7Cshk_1"><strong>Monthly subscription payment</strong></a>
        <br />
        <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick-subscriptions&business=fabbianni%40gmail.com&item_name=ShockFamily+Yearly+Subscription&currency_code=USD&notify_url=http%3A%2F%2Fwww.themeshock.com%2Fst_subscription_payment.php&t3=Y&src=1&sra=1&modify=0&a1=129&p1=1&t1=Y&a3=129&no_note=1&p3=1&no_shipping=1&return=http%3A%2F%2Fwww.themeshock.com%2Fcustomers%2Fcustomers.php&custom=aff_%7Cshk_2"><strong>Yearly subscription payment</strong></a>
    msgexp;
                            $exphtml = $msgexp;
                            $shk = 'expired';
                            break;
                    }
    
                    /*Templateshoc products*/
                    $sets [] = GetShockProducts::get_shock_products( 1 ,11, $date_format );
    
                    /*Iconshock Products*/
                    $isfinder = true;//enabled search product
                    $sets [] = GetShockProducts::get_shock_products( 6 ,4, $date_format );
    
    
                    /*Designshock products*/
                    $sets [] = GetShockProducts::get_shock_products( 23 ,6, $date_format );
    
                    /*Themeshock products*/
                    $sets [] = GetShockProducts::get_shock_products( 21 ,6, $date_format );
                    $sets [] = GetShockProducts::get_shock_products( 22 ,6, $date_format );
    
                    /*wptheme generator*/
                    //lookit
                    /* gridlayout shock */
                    $sets [] = GetShockProducts::get_shock_products( 28 ,6, $date_format );//wp edition
                    $sets [] = GetShockProducts::get_shock_products( 29 ,6, $date_format );//jquery edition
                    $sets [] = GetShockProducts::get_shock_products( 30 ,6, $date_format );//bundle edition
    
    
                    /*jQuerysslidershock*/
                    $sets [] = GetShockProducts::get_shock_products( 25 ,6, $date_format );//wp edition
                    $sets [] = GetShockProducts::get_shock_products( 26 ,6, $date_format );//jquery edition
                    $sets [] = GetShockProducts::get_shock_products( 27 ,6, $date_format );//bundle edition
                break;
                case 'is':
                case 'ds':
                case 'ts':
    
                    $cgroos = intval( $product->sales_netsales );
                    $date_format = date( "Y-m-d H:i:s" );
    
                    switch( $cgroos ){
                        case 5: // tp_2 subscription yearly promo clients
                            $date_expired = strtotime('+1 year', $sales_date );
                        break;
                        case 9: // tp_2 subscription monthly
                            $date_expired = strtotime('+1 month', $sales_date );
                        break;
                        case 29: // tp_2 recurring paymen promo clients
                            $date_expired = strtotime('+1 year', $sales_date );
                        break;
                        case 99: // tp_3 subscription yearly
                            $date_expired = strtotime('+1 year', $sales_date );
                        break;
                    }
    
                    switch ( $product->sales_ordertype ) {
                        case 'is':
                            $dmname = 'Iconshock';
                        break;
                        case 'ds':
                            $dmname = 'Designshock';
                        break;
                        case 'ts':
                            $dmname = 'Themeshock';
                        break;
                    }
                    
                    if(!isset($date_expired)){
                        $date_expired = null;
                    }

                    $purchase_history[] = array(
                                                'product_name' => $product->sales_product_name,
                                                'price' => $product->sales_netsales,
                                                'sales_date' => $product->sales_date,
                                                'expired' => $date_expired
                                               );
    
    
                    if ( $date_expired < $_SERVER['REQUEST_TIME'] ) {///verified time expired
                        $exphtml = <<<msgexp
    <p class="notify">Your $dmname subscription has expired. In order to continue, downloading current and new products, please renew your account here:</p>
    <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick-subscriptions&business=fabbianni%40gmail.com&item_name=ShockFamily+Monthly+Subscription&currency_code=USD&notify_url=http%3A%2F%2Fwww.themeshock.com%2Fst_subscription_payment.php&t3=M&src=1&sra=1&modify=0&a1=14&p1=1&t1=M&a3=14&no_note=1&p3=1&no_shipping=1&return=http%3A%2F%2Fwww.themeshock.com%2Fcustomers%2Fcustomers.php&custom=aff_%7C{$product->sales_ordertype}_1"><strong>Monthly subscription payment</strong></a>
    <br />
    <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick-subscriptions&business=fabbianni%40gmail.com&item_name=ShockFamily+Yearly+Subscription&currency_code=USD&notify_url=http%3A%2F%2Fwww.themeshock.com%2Fst_subscription_payment.php&t3=Y&src=1&sra=1&modify=0&a1=129&p1=1&t1=Y&a3=129&no_note=1&p3=1&no_shipping=1&return=http%3A%2F%2Fwww.themeshock.com%2Fcustomers%2Fcustomers.php&custom=aff_%7C{$product->sales_ordertype}_2"><strong>Yearly subscription payment</strong></a>
    msgexp;
                        $shk = 'expired';
                        break;
                    }
    
                    switch ($product->sales_ordertype) {
                        case 'is':
                            $isfinder = true;//enabled search product
                            $sets [] = GetShockProducts::get_shock_products( 6 ,4, $date_format );//iconshock code
                        break;
                        case 'ts':
                            $sets [] = GetShockProducts::get_shock_products( 21 ,6, $date_format );
                            $sets [] = GetShockProducts::get_shock_products( 22 ,6, $date_format );
                        break;
                        case 'ds':
                            $sets [] = GetShockProducts::get_shock_products( 23 ,6, $date_format );
                        break;
                    }
                break;
                    case 'tp'://subscription for templateshock
    
                        $tp = true;
                        $cgroos = intval( $product['sales_netsales'] );
                        $sales_date = strtotime ( $product['sales_date'] );
    
                        switch( $cgroos ){
                            case 9: // tp_2 subscription monthly
                                $date_expired = strtotime('+1 month', $sales_date );
                            break;
                            case 39: //tp_1 templateshock bundle 500
                            case 59: //tp_1 templateshock bundle 500
                            case 0: ///tp_1
                                $sets [] = GetShockProducts::get_shock_products( 1 ,11, $date_format );///id code temmplateshock 500
                                ///vhage to 1 ,11 becausethis promo end and this like the same
                            break 2;
                            case 99: // tp_3 subscription yearly
                                $date_expired = strtotime('+1 year', $sales_date );
                            break;
                        }
    
                        $purchase_history[] = array(
                                                    'product_name' => $product['sales_product_name'],
                                                    'price' => $product['sales_netsales'],
                                                    'sales_date' => $product['sales_date'],
                                                    'expired' => $date_expired
                                                   );
    
    
                        if ( $date_expired < $_SERVER['REQUEST_TIME'] ) {///verified time expired
                            $tp = 'expired';
                            break;
                        }
    
                        $sets [] = GetShockProducts::get_shock_products( 1 ,11, $date_format );
    
                break;
                default:
                    $sets [] = GetShockProducts::get_shock_products( $product->sales_product, $product->sales_ordertype, $date_format );
                break;
            }
    
            
            if ( $product->sales_product === '22' || $product->sales_product === '21' )
            {
    
                if ( $product['sales_reactive'] == 50 )
                    $sets[] = GetShockProducts::get_shock_products( 6 ,4, $date_format );
                else
                    $sets[] = GetShockProducts::get_shock_products( 5 ,4, $date_format );//without icon finder
            }
    
            if( $product->sales_product === '19' ) 
            {
    
                $sets [] = GetShockProducts::get_shock_products( 21 ,6, $date_format );
                $sets [] = GetShockProducts::get_shock_products( 22 ,6, $date_format );
    
                if ( $product['sales_ordertype']==="6" && count($IS_list_products_clients)==0  ) {
                    switch($product['sales_netsales']){
                        case '69':
                        case '79':
                        case '129':
                            $sets[] = GetShockProducts::get_shock_products( 5 ,4, $date_format );//without icon finder
                        break;
                    }
                }
            }
    
        }
                
        if(isset($sets)) {             
            $sets = FilterByDomain::filter_by_domain_shk($sets);
            return new SetResource($sets);
        } else {
            $sets = array('data' => ['iconshock' => false, 'designshock' => false, 'templateshock' => false]);
            return $sets;
        }
        
    }

}
        