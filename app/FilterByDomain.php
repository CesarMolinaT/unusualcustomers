<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilterByDomain extends Model
{
    public static function filter_by_domain_shk($sets) {
            
        if ( empty( $sets ) )
        return false;;
        
        $domains = array( 'iconshock','designshock','themeshock','templateshock','plugins');
        
        foreach ( $domains as $shkval) {
            $tmpsets = FilterByDomain::search( $sets,'from', $shkval );
            if ( empty( $tmpsets) ){
                $nsets[$shkval] = false;
                continue;
            }
            switch ( $shkval) {
                case 'iconshock':
                    case 'designshock':
                        case 'themeshock':
                            case 'templateshock':
                                case 'plugins':
                                    //$nsets[$shkval] = array_unique( call_user_func_array('array_merge', $tmpsets), SORT_REGULAR);
                                    
                                    $tmpsets = array_map( function( $set_pack){
                                        if ( $set_pack['single']=1 )
                                        $tmp_pack[] = $set_pack;
                                        else
                                        $tmp_pack = $set_pack;
                                        
                                        return $tmp_pack;
                                    }, $tmpsets);
                                    
                                    if ( $shkval=='plugins' or  ( current($tmpsets)['single']=1 && count($tmpsets)>1) ){
                                        $nsets[$shkval] = array_values( array_map( "unserialize", array_unique(array_map("serialize", $tmpsets ) ) ) );
                                    break;
                                }
                                
                                
                                
                                $nsets[$shkval] = call_user_func_array('array_merge', $tmpsets);
                                $nsets[$shkval] = array_filter(array_map("unserialize", array_unique(array_map("serialize", $nsets[$shkval]))));
                                
                                unset( $nsets[$shkval]['from'] );
                            break;
                            
                            default:
                            # code...
                        break;
            }
            
        }
                
    return $nsets;
        
    }

    static function search($array, $key, $value)
    {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, FilterByDomain::search($subarray, $key, $value));
            }
        }

        return $results;
    }
            
}
