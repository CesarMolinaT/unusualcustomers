<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class GetShockProducts extends Model
{
    public static function get_shock_products( $code, $type, $date_format){
        switch ( $type ) {
            case 1:
                $sets = iconshock_get_prod_by_id( $code );
                if ( empty( $sets ) )
                    return false;

                $sets['from'] ='iconshock';
            break;
            case '2':
                //$sql = $iconshock_db->prepare( "SELECT cat_code, ind_code FROM bundle where bun_code= %d;",$code );
                $sql = "SELECT cat_code, ind_code FROM bundle where bun_code=".$code;
                //$bundle = $iconshock_db->get_row( $sql, ARRAY_A);
                $bundle = DB::select($sql, [1]);

                if ( !empty( $bundle ) && is_array( $bundle ) ){

                    extract( $bundle );
                    $cod_bundle = ( empty ($cat_code ) )?$ind_code:$cat_code;
                    $sets = GetShockProducts::iconshock_get_prod_by_cat( $cod_bundle, $date_format);


                    if ( empty( $sets ) )
                        return false;

                    $sets['from'] ='iconshock';
                }

            break;
            case '3':
            case '4':
                $sets = DB::select( "SELECT 
                                        prod_code,
                                        prod_name,
                                        prod_link_tuto,
                                        prod_category,
                                        cat_name as category,
                                        cat_slug,
                                        ind_slug
                                        FROM products,categories,industries
                                        where prod_category = cat_code and 
                                        prod_industry = ind_code and 
                                        prod_code not in(1025,1008,1005) and 
                                        prod_control=1 
                                        order by prod_category;",[] );

                if ( $code == 6)
                    $sets['isfinder'] = true;

                $sets['from'] = 'iconshock';
            break;
            case '6' :
                switch ($code) {
                    case '9': /*case logo*/
                        $logo=true;
                    break;
                    case '20': /*theme generator*/
                        /* dummy */
                        $sets['tg_id'] = $_COOKIE['session']['id_user'];
                        $sets['from'] = 'themegenerator';
                    break;
                    case '21':/* Design sets*/
                        //$sets = $themeshock_db->get_results("SELECT prod_name,prod_product_path,prod_sample,cate_name as prod_format FROM products p, categories c where cate_id=cate_id_FK and prod_control=1 and prod_format not in ('wordpress') order by cate_order,prod_format,cat_counter DESC ;",ARRAY_A);
                        $sets = DB::connection('mysql_themeshock')->select("SELECT prod_name,prod_product_path,prod_sample,cate_name as prod_format FROM products p, categories c where cate_id=cate_id_FK and prod_control=1 and prod_format not in ('wordpress') order by cate_order,prod_format,cat_counter DESC", []);
                        $sets['from'] ='themeshock';
                    break;
                    case '22':/*shock bundle new*/
                        //$setsts =$themeshock_db->get_results("SELECT prod_name,prod_product_path,prod_sample,prod_format FROM products  where prod_control=1  and prod_format in ('wordpress')  order by prod_format,cat_counter DESC ;",ARRAY_A);
                        $setsts = DB::connection('mysql_themeshock')->select("SELECT prod_name,prod_product_path,prod_sample,prod_format FROM products  where prod_control=1  and prod_format in ('wordpress')  order by prod_format,cat_counter DESC", []);
                        //$setstg=$wtg_db->get_results('SELECT tg_elem_id, tg_elem_name, tg_elem_categories FROM TG_elements where tg_user_id_FK=235 and  tg_elem_status="publish" order by tg_elem_id ',ARRAY_A);
                        $setstg = DB::connection('mysql_wtg')->select('SELECT tg_elem_id, tg_elem_name, tg_elem_categories FROM TG_elements where tg_user_id_FK=235 and  tg_elem_status="publish" order by tg_elem_id', []);
                        $sets = compact( 'setsts', 'setstg');
                        $sets['from'] ='themeshock';
                    break;
                    case '23': /*aditional sets*/
                        $additional=true;
                        ///query old client
                        $query_date = <<<dsplim
    SELECT ID FROM wp_posts where post_status ='publish' and post_type='post' and post_date <= '$date_format' order by post_date desc ;
dsplim;
                        //$get_ds_ids = array_flip( array_flatten( $design_db->get_results( $query_date,ARRAY_A) ) );//get keys from ds only date specified
                        $get_ds_ids = array_flip(GetShockProducts::array_flatten( DB::connection('mysql_designshock')->select( $query_date,[]) ));
                        //$option_design=$design_db->get_row("SELECT * FROM wp_options where option_name='design_products'",ARRAY_A);
                        $option_design=DB::connection('mysql_designshock')->select("SELECT * FROM wp_options where option_name='design_products'",[1]);;
                        $p_pack=maybe_unserialize(maybe_unserialize($option_design[0]->option_value));
                        $keys_ds = array_flip(array_keys( array_diff_key( $p_pack, $get_ds_ids ) ));//ffilter keys that not used depend date intervale
                        $sets = array_diff_key( $p_pack, $keys_ds );
                        array_walk( $sets, array('self', 'id_key_assoc'));
                        krsort ( $sets );
                        $sets['from'] ='designshock';

                    break;
                    case '24': /*themeshock sets*/
                        $themeshock=true;
                        $product_pack_ts=maybe_unserialize(get_option('themeshock_products'));
                    break;
                    case '25':
                        $download = "https://www.themeshock.com/dl_add_pack.php?slidershock_jquery_version_41541841041524185410524581632.zip";
                        $prod_name = "slider shock (jquery)";
                        $detail = "http://www.jqueryslidershock.com/";
                        $sets = compact( 'download', 'prod_name', 'detail');
                        $sets['from'] ='plugins';
                    break;
                    case '26':
                        $download = "https://www.themeshock.com/dl_add_pack.php?slidershock_wordpress_premuim_450211211052140525210541612544.zip";
                        $prod_name = "slider shock (wp)";
                        $detail="http://www.jqueryslidershock.com/";
                        $sets = compact( 'download', 'prod_name', 'detail');
                        $sets['from'] ='plugins';
                    break;
                    case '27':
                        $download = "https://www.themeshock.com/dl_add_pack.php?bundle_slidershock_4521512321435132152110.zip";
                        $prod_name = "slider shock (wp and jquery)";
                        $detail = "http://www.jqueryslidershock.com/";
                        $sets = compact( 'download', 'prod_name', 'detail');
                        $sets['from'] ='plugins';
                    break;
                    case '28':
                        $download = "https://www.themeshock.com/dl_add_pack.php?gridlayoutshock_4541521065412185412415415415.zip";
                        $prod_name = "Grid layout shock (wp)";
                        $detail = "http://www.gridlayoutshock.com/";
                        $sets = compact( 'download', 'prod_name', 'detail');
                        $sets['from'] ='plugins';
                    break;
                    case '29':
                        $download = "https://www.themeshock.com/dl_add_pack.php?Gridlayoutshock_jQuery_version_452487964135.zip";
                        $prod_name = "Grid layout shock (jquery)";
                        $detail = "http://www.gridlayoutshock.com/";
                        $sets = compact( 'download', 'prod_name', 'detail');
                        $sets['from'] ='plugins';
                    break;
                    case '30':
                        $download ="https://www.themeshock.com/dl_add_pack.php?Gridlayoutshock_bundle_version_546821379546.zip";
                        $prod_name ="Grid layout shock (wp and jquery)";
                        $detail ="http://www.gridlayoutshock.com/";
                        $sets = compact( 'download', 'prod_name', 'detail');
                        $sets['from'] ='plugins';
                    break;
                    case '31':
                    case '32':
                    case '33':
                    case '34':
                    case '35':
                    case '58':
                    case '59':
                    case '60':
                    
                        /*Aqui va mobile icons*/
                        switch ($code){
                            case '31':
                                $mobile_icons_name='Mobile icons pack';
                                $iscat='51,53,54,55,58,59,60';
                            break;
                            case '32':
                                $mobile_icons_name='Special icons pack';
                                $iscat='21,27,52,56,57,58';//21 and 37 clean and golden 
                                /*
                                58 incluido temporalmente mientre se arregla el tema de material por el tema de
                                este cliente benod1@gmail.com or o que material era parte de special
                                */
                                
                            break;
                            case '33':
                                $mobile_icons_name='Special plus mobile icons';
                                $iscat='21,27,51,52,53,54,55,56,57,58,59,60';
                            break;
                            case '34':
                                $mobile_icons_name='Shock icon fonts';
                                $iscat='56';
                            break;
                            case '35':
                                $mobile_icons_name='Flat icons';
                                $iscat='57';
                            break;
                            case '58':
                                $iscat='58';
                                $mobile_icons_name='Material icons';
                            break;
                            case '59':
                                $iscat='59';
                                $mobile_icons_name='Outlined Icons';
                            break;
                            case '60':
                                $iscat='60';
                                $mobile_icons_name='Windows 10 Icons';
                            break;
                        }

                        $sets = GetShockProducts::iconshock_get_prod_by_cat( $iscat, $date_format);
                        $sets['from'] ='iconshock';
                    break;
                    case '193': /*Iphone*/
                        $sets = GetShockProducts::iconshock_get_prod_by_cat( 55, $date_format);
                        $sets['from'] ='iconshock';
                    break;
                    case '194': /*windows 8*/
                        $sets = GetShockProducts::iconshock_get_prod_by_cat( 54, $date_format);
                        $sets['from'] ='iconshock';
                    break;
                    case '195': /* android */
                        $sets = GetShockProducts::iconshock_get_prod_by_cat( 53, $date_format);
                        $sets['from'] ='iconshock';
                    break;
                    case '4918': //line icons inserted temporaly
                        $prod_category ='gen';
                        $url ='https://www.iconshock.com/line-icons/';
                        $prod_link_tuto ='../final_products/Free_outline_iconset.7z';
                        $prod_name ='Line Icons';
                        $sets = compact( 'prod_category','url', 'prod_link_tuto', 'prod_name');
                        $sets['from'] ='iconshock';
                    break;
                }

            break;
            case '7':
                //$option_design=$design_db->get_row("SELECT * FROM wp_options where option_name='design_products'",ARRAY_A);
                $option_design = DB::connection('mysql_designshock')->select("SELECT * FROM wp_options where option_name='design_products'", []);
                $product_pack=maybe_unserialize(maybe_unserialize($option_design[0]->option_value));
                $pack =$product_pack[$code];
                $id = $code;
                $url ='https://www.designshock.com/?p='.$code;
                if (is_array( $pack))
                    extract( $pack);
                $single = 1;
                $sets[0] = compact( 'id', 'url', 'ds_btn_paypal','ds_zip_full','single');
                $sets['from'] ='designshock';
            break;
            case '8':
                $product_pack_ts=maybe_unserialize(get_option('themeshock_products'));
                $pack = $product_pack_ts[$code];
                $tg_elem_id = (int)$code;
                $detail_link ='https://www.themeshock.com/?p='.$code;
                if ( $pack['tg_post']===true ) {
                    $detail_link = str_replace('builder/','',$pack['tg_url']);
                    $prod_name = get_the_title( $code );
                    $prod_format = "Themegenerator themes";
                }
                $single = 1;

                //$sets = compact( 'id','url','ds_zip_full','ds_btn_paypal','pack' );
                $sets = compact( 'tg_elem_id', 'detail_link','prod_name', 'prod_format','single');
                $sets['from'] ='themeshock';
            break;
            case '9':
                //$sets = $templates_wp->get_row( $templates_wp->prepare( "SELECT post_title,post_id,meta_value FROM wp_tmpl_posts, wp_tmpl_postmeta  where post_id = %d and ID=post_id and meta_key='_post_download_attachment' and post_status='publish';", $code ), ARRAY_A );
                $sets = DB::connection('mysql_templateshock')->select("SELECT post_title,post_id,meta_value FROM wp_tmpl_posts, wp_tmpl_postmeta  where post_id = $code and ID=post_id and meta_key='_post_download_attachment' and post_status='publish'", [1]);
                //extract( $sets);
                $post_title = $sets[0]->post_title;
                $post_id = $sets[0]->post_id;
                $meta_value = $sets[0]->meta_value;
                $single = 1;
                $sets[0] = compact( 'post_title', 'post_id','meta_value', 'single');
                $sets['from'] ='templateshock';
            break;
            case '10':
                $term_id = 0;
                switch ( $code ) {
                    case '7959'://Letter Templates
                        $term_id = 17;
                    break;
                    case '4544': //Quotation templtates	
                        $term_id = 16;
                    break;
                    case '2707'://Invoice templates
                        $term_id = 13;
                    break;
                    case '1953'://Tri-fold templates
                        $term_id = 15;
                    break;
                    case '832'://Folder templates
                        $term_id = 9;
                    break;
                    case '69'://Flyer Templates
                        $term_id = 11;
                    break;
                    case '67':///Business Card Templates
                        $term_id = 7;
                    break;
                    case '61'://Proposal Templates
                        $term_id = 14;
                    break;
                    case '59'://CV templates
                        $term_id = 10;
                    break;
                    case '57':// menu templates
                        $term_id = 12;
                    break;
                    case '55': //poster templates
                        $term_id = 8;
                    break;
                    case '53': // logo templates
                        $term_id = 18;
                    break;
                    case '16724': // full branding templates
                        $term_id = 19;
                    break;
                }
                
                if ( empty( $term_id ) )
                    break;
                
                $query_tmp = "SELECT object_id FROM wp_tmpl_term_relationships where term_taxonomy_id=$term_id"; ///query templates for bundle
                //$get_ids = array_flatten( $templates_wp->get_results( $query_tmp, ARRAY_A ) );
                $get_ids = GetShockProducts::array_flatten2( DB::connection('mysql_templateshock')->select($query_tmp, []) );
                $tpids = implode( ',', $get_ids );
                //$sets = $templates_wp->get_results( "SELECT post_title,post_id,meta_value FROM wp_tmpl_posts, wp_tmpl_postmeta  where post_id in( $tpids ) and post_date <= '$date_format' and ID=post_id and meta_key='_post_download_attachment' and post_status='publish';", ARRAY_A );
                $sets = DB::connection('mysql_templateshock')->select("SELECT post_title,post_id,meta_value FROM wp_tmpl_posts, wp_tmpl_postmeta  where post_id in( $tpids ) and post_date <= '$date_format' and ID=post_id and meta_key='_post_download_attachment' and post_status='publish'", []);
                $sets['from'] ='templateshock';
            break;
            case '11':
                //$sets = $templates_wp->get_results( $templates_wp->prepare( "SELECT post_title,post_id,meta_value FROM wp_tmpl_posts, wp_tmpl_postmeta  where post_type='post' and post_date <= '$date_format' and ID=post_id and meta_key='%s' and post_status='publish';", '_post_download_attachment' ), ARRAY_A );
                $sets = DB::connection('mysql_templateshock')->select("SELECT post_title,post_id,meta_value FROM wp_tmpl_posts, wp_tmpl_postmeta  where post_type='post' and post_date <= '$date_format' and ID=post_id and meta_key='_post_download_attachment' and post_status='publish'", [1]); 
                $sets['from'] ='templateshock';
            break;
            case '12':
                $download = "https://www.themeshock.com/dl_add_pack.php?slidershock_wordpress_premuim_450211211052140525210541612544.zip";
                $prod_name = "Templateshock bundle 500";
                $detail="https://www.templateshock.com/";
                $prod_category = 'Other';
                $linkct = "https://www.templateshock.com/";
                $sets = compact( 'download', 'prod_name', 'detail', 'prod_category', 'linkct');
                $sets['from'] ='templateshock';
            break;
            default:
                # code...
            break;
        }
        
        return $sets;
    }

    static function array_flatten($array) { 
        if (!is_array($array)) { 
          return false; 
        } 
        $result = array(); 
        foreach ($array as $key => $value) { 
          if (is_array($value)) { 
            $result = array_merge($result, GetShockProducts::array_flatten($value)); 
          } else { 
            $result = array_merge($result, array($key => $value->ID));
          } 
        } 
        return $result; 
    }

    static function array_flatten2($array) { 
        if (!is_array($array)) { 
            return false; 
        } 
        $result = array(); 
        foreach ($array as $key => $value) { 
            if (is_array($value)) { 
            $result = array_merge($result, GetShockProducts::array_flatten($value)); 
            } else { 
            $result = array_merge($result, array($key => $value->object_id));
            } 
        } 
        return $result; 
    }

    static function id_key_assoc( &$val, $key){
		$val['id'] = $key;
    }
    
    static function iconshock_get_prod_by_cat( $cat_codes, $date_format, $cat_mode=true ){

		if ( $cat_mode )
			$mode = "prod_category in ( $cat_codes ) and";
		else
			$mode = "prod_industry in ( $cat_codes ) and";

		$sets = DB::select("SELECT DISTINCT
												prod_code,
												prod_name,
												prod_link_tuto,
												prod_category,
												cat_name as category,
												cat_slug,
												prod_industry
												FROM products,categories,industries
												where prod_category = cat_code and 
												$mode
												prod_code not in(1025,1008,1005,1046) and
												prod_control=1 and
												prod_dateup <='$date_format' order by prod_category",
										   []);
		return $sets;
	}
}

